import { StyleSheet, Text, View, Alert } from 'react-native'
import React, {useEffect} from 'react'
import messaging from '@react-native-firebase/messaging'

const App = () => {
  const getToken = async() => {
    const token = await messaging().getToken()
    console.log(JSON.stringify(token))
  }
  
  useEffect(()=>{  
    getToken()
  }, [])

  return (
    <View>
      <Text>Firebase messaging notification</Text>
    </View>
  )
}

export default App

const styles = StyleSheet.create({})